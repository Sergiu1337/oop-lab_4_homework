#pragma once
#include<ostream>
#include<istream>
using namespace std;

template <class Type>
class DynamicArray
{
public:

	DynamicArray();

	~DynamicArray();

	DynamicArray(int cap, int len, Type* data);

	Type& operator[](int index);

	DynamicArray& operator=(const DynamicArray& c);

	friend ostream& operator<< <>(ostream& os, const DynamicArray<Type>& c);

	void insert(Type elem);

	void deleteLast();

	int getSize();

	int getCapacity();

	void printStaticData();

private:
	int cap;
	int len;
	Type* data;
};

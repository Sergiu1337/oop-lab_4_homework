#include "complex.h"

Complex::Complex() {
	m_real = 0;
	m_imaginary = 0;
}

Complex::Complex(float real, float imaginary) {
	m_real = real;
	m_imaginary = imaginary;
}

Complex Complex::add(Complex x) {
	Complex res{ this->m_real + x.m_real, m_imaginary + x.m_imaginary };
	return res;
}

Complex Complex::operator+(const Complex& x) {
	Complex res{ this->m_real + x.m_real, m_imaginary + x.m_imaginary };
	return res;
}

void Complex::setReal(float r)
{
	m_real = r;
}

void Complex::setImaginary(float i)
{
	m_imaginary = i;
}

ostream& operator<<(ostream& os, const Complex& c) {
	os << c.m_real << " + " << c.m_imaginary << "i" << endl;
	return os;
}

istream& operator>>(istream& is, Complex& c)
{
	float r;
	is >> r;
	c.setReal(r);
	float i;
	is >> i;
	c.setImaginary(i);

	return is;
}



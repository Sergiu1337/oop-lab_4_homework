#pragma once
#include<ostream>
#include<istream>
using namespace std;

class Complex
{
public:
	Complex();
	Complex(float real, float imaginary);

	Complex add(Complex x);

	Complex operator+(const Complex& c);

	friend ostream& operator<<(ostream& os, const Complex& c);

	void setReal(float r);
	void setImaginary(float i);

private:
	float m_real;
	float m_imaginary;
};

istream& operator>>(istream& is, Complex& c);

#include "dynamic_array.cpp"
#include "complex.h"
#include<stdlib.h>
#include<iostream>
#include<fstream>
#include<cstring>
#include <crtdbg.h>
using namespace std;

int main() {
	DynamicArray<Complex> ComplexArray;

	ifstream file;
	file.open("complex_numbers.txt");
	float real, imaginary;

	while (file >> real >> imaginary) {
		ComplexArray.insert(Complex(real, imaginary));
	}
	cout << ComplexArray;

	cout << "The sum is: " << ComplexArray.getSum();
	
	_CrtDumpMemoryLeaks();

	return 0;
}

#pragma once
#include<ostream>
#include<istream>
using namespace std;

// Had to move the headers here because of using templates

template <class Type>
class DynamicArray
{
public:

	DynamicArray();

	~DynamicArray();

	DynamicArray(int cap, int len, Type* data);

	Type& operator[](int index);

	DynamicArray& operator=(const DynamicArray& c);

	friend ostream& operator<< <>(ostream& os, const DynamicArray<Type>& c);

	void insert(Type elem);

	void deleteLast();

	int getSize();

	int getCapacity();

	void printStaticData();
	
	Type getSum();

private:
	int cap;
	int len;
	Type* data;
};

#include<iostream>
using namespace std;

static int instancesCreated = 0, copyCalls = 0, assignmentCalls = 0;

template <class Type>
DynamicArray<Type>::DynamicArray()
{
	instancesCreated++;

	this->data = new Type[10];
	this->cap = 10;
	this->len = 0;
}

template<class Type>
DynamicArray<Type>::~DynamicArray()
{
	delete[] this->data;
}


template <class Type>
DynamicArray<Type>::DynamicArray(int capacity, int length, Type* new_data)
{
	copyCalls++;

	this->cap = capacity;
	this->len = length;
	this->data = new_data;
}

template <class Type>
Type& DynamicArray<Type>::operator[](int index)
{
	if (index >= this->len) {
		cout << "Array index out of bounds, exiting";
		exit(0);
	}
	else {
		Type& value = this->data[index];
		return this->data[index];
	}
}

template <class Type>
DynamicArray<Type>& DynamicArray<Type>::operator=(const DynamicArray<Type>& c)
{
	assignmentCalls++;

	if (this != &c) {		// Check that we don't overwrite the same variable
		this->cap = c.cap;
		this->len = c.len;
		for (int i = 0; i < c.len; i++) {
			this->data[i] = c.data[i];
		}
	}
	return *this;
}

template <class Type>
ostream& operator<<(ostream& os, const DynamicArray<Type>& c)
{
	for (unsigned int i = 0; i < c.len; i++) {
		os << c.data[i] << "\n";
	}
	os << endl;
	return os;
}

template <class Type>
void DynamicArray<Type>::insert(Type elem)
{
	Type* auxArray;
	if (this->len + 1 == this->cap) {
		this->cap += 10;
		auxArray = new Type[this->cap];
		for (int i = 0; i < this->len; i++)
			auxArray[i] = this->data[i];
		delete[] this->data;
		this->data = auxArray;
	}
	this->data[this->len++] = elem;
}


template <class Type>
void DynamicArray<Type>::deleteLast()
{
	Type auxArray[this->cap];
	for (int i = 0; i < this->len - 1; i++) {
		auxArray[i] = this->data[i];
	}
	delete[] this->data;
	this->cap--;
	this->len--;
	this->data = auxArray;
}

template <class Type>
int DynamicArray<Type>::getSize() {
	return this->len;
}

template <class Type>
int DynamicArray<Type>::getCapacity() {
	return this->cap;
}

template <class Type>
void DynamicArray<Type>::printStaticData() {
	cout << "Instances = " << instancesCreated << endl;
	cout << "copyCalls = " << copyCalls << endl;
	cout << "assignmentCalls = " << assignmentCalls << endl;
}

template<class Type>
Type DynamicArray<Type>::getSum()
{
	Type sum;
	for (int i = 0; i < this->getSize(); i++)
		sum = this->data[i] + sum;
	return sum;
}
